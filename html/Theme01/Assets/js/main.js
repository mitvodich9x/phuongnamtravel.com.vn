﻿//custom slick slider
$(".sslider").slick({
	arrows: false,
	dots: true,
	appendDots: $(".sslider-dots")
});

$(".review-slick").slick({
	arrows: false,
	dots: true,
	appendDots: $(".review-dots")
});

$(".news__slick").slick({
	arrows: false,
	dots: true,
	appendDots: $(".news__dots")
});

$(".gioithieu-slick").slick({
	arrows: false,
	dots: true
});

$(".foundTour-slick").slick({
	appendArrows: $(".fT-arrows"),
	mobileFirst: true,
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 3,
	arrows: false,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 6,
			arrows: true
		}
	}, {
		breakpoint: 600,
		settings: {
			slidesToShow: 4,
			arrows: true
		}
	}]
});

$(".onpost-more").slick({
	mobileFirst: true,
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 1,
	arrows: false,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 3,
			arrows: true
		}
	}, {
		breakpoint: 600,
		settings: {
			slidesToShow: 2,
			arrows: true
		}
	}]
});

$(".tin-new__more").slick({
	mobileFirst: true,
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 1,
	arrows: false,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
			arrows: true
		}
	}, {
		breakpoint: 600,
		settings: {
			slidesToShow: 2,
			arrows: true
		}
	}]
});
 
$(".tin-new__slider").slick({
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 1
});

$('.ops-big').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	fade: true,
	asNavFor: '.ops-small'
});
$('.ops-small').slick({
	slidesToShow: 5,
	slidesToScroll: 1,
	asNavFor: '.ops-big',
	dots: false,
	focusOnSelect: true,
	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>'
});

//show menu tablet & mobile
let isToggle = false;
$("#btnShowMenu").click(function () {
	if (isToggle) {
		$("#menu").css({
			'transform': 'translateY(20px)',
			'opacity': '0',
			'visibility': 'hidden'
		});
		isToggle = !isToggle;
	} else {
		$("#menu").css({
			'transform': 'translateY(0)',
			'opacity': '1',
			'visibility': 'visible'
		});
		isToggle = !isToggle;
	}
});